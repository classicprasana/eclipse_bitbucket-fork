package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EclipseBitbucketApplication {

	public static void main(String[] args) {
		SpringApplication.run(EclipseBitbucketApplication.class, args);
		
		System.out.println("bitbucket test file modified ");
		System.out.println("chnaged by dev2 and pushed to remote repo");
		System.out.println("After fork :: modified by dev2 ");
	}

}
